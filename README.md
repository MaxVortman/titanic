# Titanic

We want to fill missing values in the 'Age' column  in the 'Titanic' 
dataset with median values calculated depending on the prefix in 'Name' column:

For each title from
```
 ["Mr.", "Mrs.", "Miss."]
 ```
We need to use median value calculated for every group. 
All entities where 'Name' doesn't contain the title from the list above should be marked with title = "NaN".
Find the number of missed values and median values corresponding to every of these 4 groups ('Mr.', 'Mrs.''Miss.', 'NaN'). Provide the answer in the form - a list of tuples (Title of the group, number of missed values for the group, median value calculated for the 
group). The pattern for the answer is 
```
[('Mr.', 123, 23), ('Mrs.', 234, 34), ('Miss.', 321, 21), ('NaN', 19, 91)]
```
The median values should be rounded to the nearest integer.

_Note: Please be carefull with dot in titles.
       'Titanic' dataset you can find in folder data/ under the name train.csv_
