import pandas as pd
import numpy as np

def get_filled():
    # Load the Titanic dataset
    titanic_df = pd.read_csv('data/train.csv')

    # Define the list of titles to consider
    titles_to_consider = ["Mr", "Mrs", "Miss"]

    # Function to extract and clean titles
    def extract_title(name):
        title = name.split(',')[1].split('.')[0].strip()
        return title + "." if title in titles_to_consider else "NaN"

    # Apply the function to create a 'Title' column
    titanic_df['Title'] = titanic_df['Name'].apply(extract_title)

    # Calculate the number of missing values for each group
    missing_values_count = titanic_df.groupby('Title')['Age'].apply(lambda x: x.isin([0, np.nan]).sum()).to_dict()

    # Group the dataset by title and calculate the median age for each group
    median_values = titanic_df.groupby('Title')['Age'].median().round().to_dict()

    # Create a list of tuples with the desired information
    result = [(title, missing_values_count[title], int(median_values[title])) for title in ('Mr.', 'Mrs.', 'Miss.', 'NaN')]

    return result